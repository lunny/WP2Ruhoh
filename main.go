package main

import (
	"encoding/xml"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/lunny/html2md"
)

type Category struct {
	Id   int    `xml:"wp_term_id"`
	Name string `xml:"wp_cat_name"`
}

type Category2 struct {
	Domain   string `xml:"domain,attr"`
	Nickname string `xml:"nicename,attr"`
	Content  string `xml:",chardata"`
}

type Item struct {
	Id       int    `xml:"wp_post_id"`
	ParentId int    `xml:"wp_post_parent"`
	Title    string `xml:"title"`
	Link     string `xml:"link"`
	PubDate  string `xml:"pubDate"`
	//Creator string `xml:"dc:creator"`
	Url        string      `xml:"guid"`
	Content    string      `xml:"content_encoded"`
	PostDate   string      `xml:"wp_post_date"`
	Type       string      `xml:"wp_post_type"`
	Attachment string      `xml:"wp_attachment_url"`
	Desc       string      `xml:"description"`
	Categories []Category2 `xml:"category"`
	Md         string      `xml:"-"`
}

type Tag struct {
	Id   int    `xml:"wp_term_id"`
	Name string `xml:"wp_tag_name"`
}

type Result struct {
	XMLName    xml.Name   `xml:"rss",json:"rss"`
	Title      string     `json:"title",xml:"channel>title"`
	Link       string     `xml:"channel>link"`
	Desc       string     `xml:"channel>description"`
	PubDate    string     `xml:"channel>pubDate"`
	Lang       string     `xml:"channel>language"`
	Categories []Category `xml:"channel>wp_category"`
	Generator  string     `xml:"channel>generator"`
	Items      []Item     `json:"item" xml:"channel>item"`
	Tags       []Tag      `xml:"channel>wp_tag"`
}

func Parse(filename string) (Result, error) {
	v := Result{}

	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return v, err
	}

	str := string(data)
	str = strings.Replace(str, "<wp:", "<wp_", -1)
	str = strings.Replace(str, "</wp:", "</wp_", -1)

	str = strings.Replace(str, "<content:", "<content_", -1)
	str = strings.Replace(str, "</content:", "</content_", -1)

	err = xml.Unmarshal([]byte(str), &v)
	if err != nil {
		return v, err
	}

	return v, err
}

const (
	TPL_NEW_PAGE = `---
title: %s
date: '%s'
description: %s
---

%s`

	TPL_NEW_POST = `---
title: %s
date: '%s'
description: %s
tags: %s
categories: %s
---

%s
`
)

func ToMd(result *Result) error {
	for _, item := range result.Items {
		if strings.ToLower(item.Type) == "attachment" {

		}
	}

	return nil
}

func transTitle(title string) string {
	title = strings.Replace(title, " ", "-", -1)
	return strings.Replace(title, "/", "-", -1)
	return title
}

func transPost(item *Item, postsDir string) error {
	title := transTitle(item.Title)

	f, err := os.OpenFile(filepath.Join(postsDir, title+".md"), os.O_CREATE|os.O_WRONLY, 0777)
	if err != nil {
		return err
	}
	defer f.Close()

	var cates, tags string
	ms := make([]string, 0)
	ms2 := make([]string, 0)
	if len(item.Categories) > 0 {
		for _, s := range item.Categories {
			if s.Domain == "category" {
				ms = append(ms, strings.Replace(s.Content, "&amp;", "&", -1))
			} else if s.Domain == "post_tag" {
				ms2 = append(ms2, strings.Replace(s.Content, "&amp;", "&", -1))
			}
		}

		if len(ms) > 0 {
			cates = "['" + strings.Join(ms, "' '") + "']"
		}
		if len(ms2) > 0 {
			tags = "['" + strings.Join(ms2, "' '") + "']"
		}
	}

	tm, err := time.Parse(time.RFC1123Z, item.PubDate)
	if err != nil {
		return err
	}

	md := fmt.Sprintf(TPL_NEW_POST,
		strings.Replace(item.Title, "&amp;", "&", -1),
		tm.Format("2006-01-02 15:04:05"), item.Desc,
		tags, cates, html2md.Convert(item.Content))

	f.WriteString(md)
	return nil
}

func transPage(item *Item, pagesDir string) error {
	title := transTitle(item.Title)

	f, err := os.OpenFile(filepath.Join(pagesDir, title+".md"), os.O_CREATE|os.O_WRONLY, 0777)
	if err != nil {
		return err
	}
	defer f.Close()

	tm, err := time.Parse(time.RFC1123Z, item.PubDate)
	if err != nil {
		return err
	}

	md := fmt.Sprintf(TPL_NEW_PAGE,
		strings.Replace(item.Title, "&amp;", "&", -1),
		tm.Format("2006-01-02 15:04:05"), item.Desc,
		html2md.Convert(item.Content))

	f.WriteString(md)
	return nil
}

func transAttachment(item *Item, mediasDir string) error {
	idx := strings.Index(item.Url, "uploads")
	relatePath := item.Url[idx+len("uploads")+1:]
	d := filepath.Dir(relatePath)
	fName := filepath.Base(relatePath)

	newDir := filepath.Join(mediasDir, d)
	os.MkdirAll(newDir, os.ModePerm)
	destPath := filepath.Join(newDir, fName)
	fmt.Println("downloading", item.Url, "->", destPath)

	resp, err := http.Get(item.Url)
	if err != nil {
		return err
	}

	f, err := os.OpenFile(destPath, os.O_CREATE|os.O_WRONLY, 0777)
	if err != nil {
		return err
	}
	defer f.Close()

	_, err = io.Copy(f, resp.Body.(io.Reader))
	return err
}

func main() {
	v, err := Parse("wordpress.2013-04-18.xml")
	if err != nil {
		fmt.Printf("error: %v", err)
		return
	}

	/*fmt.Printf("XMLName: %#v\n", v.XMLName)
	  fmt.Printf("Title: %q\n", v.Title)
	  fmt.Printf("Link: %q\n", v.Link)
	  fmt.Printf("Desc: %v\n", v.Desc)
	  fmt.Printf("PubDate: %v\n", v.PubDate)
	  fmt.Printf("Lang: %v\n", v.Lang)
	  fmt.Printf("Categories: %v\n", v.Categories)
	  fmt.Printf("Generator: %v\n", v.Generator)
	  fmt.Printf("Items: %v\n", v.Items)*/

	counts := make(map[string]int)
	for _, item := range v.Items {
		counts[item.Type] = counts[item.Type] + 1
	}

	for t, c := range counts {
		fmt.Printf("%v : %v\n", t, c)
	}

	//html2md.AddRule("img", html2md.SameRule("img", html2md.Void))

	ToMd(&v)

	curPath, err := os.Getwd()
	if err != nil {
		fmt.Println(err)
		return
	}

	for _, item := range v.Items {
		switch item.Type {
		case "post":
			postsDir := filepath.Join(curPath, "posts")
			os.MkdirAll(postsDir, os.ModePerm)
			err = transPost(&item, postsDir)
			if err != nil {
				fmt.Println(err)
				return
			}
		case "page":
			pagesDir := filepath.Join(curPath, "pages")
			os.MkdirAll(pagesDir, os.ModePerm)
			err = transPage(&item, pagesDir)
			if err != nil {
				fmt.Println(err)
				return
			}
		case "attachment":
			mediaDir := filepath.Join(curPath, "media")
			os.MkdirAll(mediaDir, os.ModePerm)
			err = transAttachment(&item, mediaDir)
			if err != nil {
				fmt.Println(err)
				return
			}
		}
	}
}
